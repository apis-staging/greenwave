library(tidyverse)
library(greenwave)

# TODO: write this as a file to the output dir so it is easily used
# cache_dir <- 'sandbox/jornada-rs/e7f10d23'  # 6a3e7d21
cache_dir <- 'cache/rio-mora/dc0bceeb'  #'cache/apis-synth/pc/6643c4ad'

opts <- readRDS(file.path(cache_dir, 'opts.meta'))
# sim <- readRDS(file.path(cache_dir, 'sim.data'))
resp_data <- read_csv(file.path(cache_dir, 'data', 'Y-all-parts.csv'))
draws <- readRDS(file.path(cache_dir, 'draws.ckpt'))


diagnostics <- gw_diagnostics(draws, cache_dir = opts$cache)

add_params <- greenwave:::param_names()  # c('gamma1_R', 'gamma2', 'omega1_R')
# dqs <- gw_dqs(draws, what = 'fixed_plus_random', other_params = add_params)
dqs <- gw_dqs(draws, other_params = add_params, cache_dir = opts$cache)
# dqs <- readRDS(file.path(cache_dir, 'dqs', 'dqs.data'))
dqs_tidy <- gw_dqs_tidy(dqs)

dqs_plot <- dqs_tidy$fixed_plus_random %>%
  filter(!dq %in% c('alpha1', 'alpha2', add_params)) %>%
  gw_vis_dqs(opts$cache, lab_width = 16)  # facet_scales = 'fixed'
dqs_plot

# get('time_elapsed_total', attr(draws, 'fit_info'))
# ls(envir = attr(draws, "fit_info"))

gw_vis_traces(draws)
gw_vis_traces(draws, coefs = 'random')


# draws_re <- calc_random_effects(draws)
# str(draws_re)
# draws_re$sd_kappa_alpha1 %>% str
# mcmc_intervals(draws_re$sd_kappa_alpha1, facet_args = list(nrow = 2))

grp_dims <- mget(c("J", "K"), attr(draws, 'fit_info'))


# z50 <- HDInterval::hdi(draws_re$sd_kappa_alpha1, credMass = 0.5)
this_re <- 'sd_kappa_gamma2'
z_hdis <- lapply(c(0.5, 0.9), function(x) {
  z_hdi <- HDInterval::hdi(draws_re[[this_re]], credMass = x)
  dimnames(z_hdi)[[1]] <- sprintf('i%s_%s', x * 100, dimnames(z_hdi)[[1]])
  z_hdi
})
z_median <- apply(as.matrix(draws_re[[this_re]]), 2, median)
d_stats <- do.call(rbind, c(z_hdis, list(median = z_median))) %>%
  as_tibble(rownames = 'stat') %>%
  gather(param, val, -stat) %>%
  mutate(jk_idx = as.numeric(str_extract(param, "(?<=\\[).*(?=,)"))) %>% as.data.frame

# apply(z, MARGIN = 2, quantile, probs = c(0))

# J = 5; K = 11
d_housekeeping <- tibble(loc_idx = rep(1:grp_dims$J, times = grp_dims$K),
            year = rep(1:grp_dims$K, each = grp_dims$J)) %>%
  mutate(jk_idx = 1:n())

d_interval <- d_stats %>% left_join(d_housekeeping, by = 'jk_idx') %>%
  spread(stat, val)
ggplot(d_interval) +
  facet_grid(loc_idx ~ .) +
  geom_hline(yintercept = 0, linetype = 'dashed', alpha = 0.5) +
  geom_linerange(aes(x = year, ymin = i90_lower, ymax = i90_upper), size = 1) +
  geom_linerange(aes(x = year, ymin = i50_lower, ymax = i50_upper), size = 2) +
  geom_point(aes(x = year, y = median), pch = 21, fill = 'white', size = 3)

ppds <- gw_ppd(draws)
pred <- gw_vis_fit(ppds, opts, n_yhat_draws = 100, cache_dir = NULL)

training_set <- resp_data %>% filter(partition == 'training')
gw_vis_fit(draws, opts, n_yhat_draws = 100, #cache_dir = yup,
           Y = resp_data, j = 1:4, k = 4:11)  #unique(training_set$loc_idx)

gw_vis_po(draws, opts, n_yhat_draws = 100, cache_dir = yup,
          Y = resp_data)
