devtools::install_github("docopt/docopt.R")
devtools::install(quick = TRUE, upgrade = 'never')
library(tidyverse)
library(lubridate)
library(greenwave)

# ---- helpers ----
find_skip <- function(file, pattern) {
  min(grep(pattern, readr::read_lines(file)))
}
my_reader <- function(file, pattern, ...) {
  readr::read_csv(file, skip = find_skip(file, pattern) - 1) %>% 
    mutate(site = sub('_3day.csv', '', basename(file)))
}

seed <- 123  # controls the data partition
n_locs <- 2  # number of locations
n_years <- 7  # number of years

n_burnin <- 70000
n_iter <- 10000
n_chains <- 5  # number of MCMC chains to run

# eo_data <- 'MODIS_ndvi_data grass pixels.csv'  
# sensor <- tolower(strsplit(eo_data, '_')[[1]][1])

fxnl_grp <- 'GR'

cache <- file.path('/datadrive/cache/synth-paper', fxnl_grp)  
# assets_path <- '/datadrive/synth-paper'  

re_prior_dist = 'cauchy'  # or 'flat' (the default) or 'cauchy'
reg_on_sigma = 'logit'

gcc_re_sf <-
  "c(lapply(greenwave:::get_re_sf_defaults()[c('alpha1')], function(x) x * 35),
   lapply(greenwave:::get_re_sf_defaults()[c('delta1', 'delta2')], function(x) x * 2),
   lapply(greenwave:::get_re_sf_defaults()[c('gamma2')], function(x) x * 5),
 lapply(greenwave:::get_re_sf_defaults()[c('lambda')], function(x) x * 3))"
gcc_priors <-
  "list(alpha1 = list(mean = 200, sd = 50),
       delta1 = list(mean = log(1/30), sd = greenwave:::get_sd_delta(30, shift = 15, log = TRUE)),
       delta2 = list(mean = log(1/30), sd = greenwave:::get_sd_delta(30, shift = 15, log = TRUE)),  # 
       gamma1_R = list(mean = 0.34, sd = 0.025),
       gamma2 = list(mean = 0.36, sd = 0.025),
       lambda = list(mean = log(250-200), sd = 0.5))"  # plot(density(exp(rnorm(1E4, log(275-200), 0.5))))

opts <- gw_opts(
  seed = seed,
  n_locs = n_locs, n_years = n_years,
  use_single_spacetime = TRUE,
  excl_params = 'c("omega2", "omega1_R")',  #   
  n_burnin = n_burnin, n_iter = n_iter, n_chains = n_chains,
  resp_data = 'none', cov_data = 'none',  # file.path(assets_path, eo_data)
  cache = cache, omit_sys_time = TRUE,
  # loc_attr = "pixel", dttm_attr = 'date',
  p_parts = c(training = 0.80, test = 0.20),
  re_sf = gcc_re_sf,
  reg_on_sigma = reg_on_sigma
  # re_prior_dist = re_prior_dist,
  # priors = gcc_priors
)
print(opts)

# Load response and covariate data.
which_vi <- "gcc_90"

files <- list.files('/datadrive/synth-paper/phenocam', 
                    pattern = '(GR|SH)_[0-9]+_3day.csv$',
                    full.names = TRUE)

d <- purrr::map_dfr(files, my_reader, pattern = "^date") %>% 
  filter(grepl(fxnl_grp, site), !grepl('ibp0', site))

resp_data <- d %>% 
  drop_na(gcc_90, date) %>% 
  filter(date <= ymd('2020-07-02')) %>% 
  rename(location = site) %>% 
  gw_partition_vi(opts$cov_data, opts, vi = !!which_vi) #%>%
# group_by(loc_id, day) %>%  # this is new!
# sample_n(1) %>%
# ungroup()

Y_in_sample <- resp_data %>% filter(partition == 'training')  # dim(Y_in_sample)

obs <- gw_vis_obs(resp_data, cache_dir = opts$cache, file_ext = 'png')  # obs[[1]]

draws_ckpt <- file.path(opts$cache, 'draws.ckpt')
draws_extra_ckpt <- file.path(opts$cache, 'draws-extra.ckpt')
draws <- if (file.exists(draws_ckpt)) {
  if (file.exists(draws_extra_ckpt)) readRDS(draws_extra_ckpt) else readRDS(draws_ckpt)
} else {
  gw_fit(Y_in_sample, Xs = NULL, opts)
}

traces <- gw_vis_traces(draws, coefs = 'fixed', cache_dir = opts$cache, file_ext = 'png')


sf_eval <- gw_param_scaled_dists(draws, cache_dir = opts$cache, file_ext = 'png')

# Model evaluation
ppds_data <- file.path(opts$cache, 'pred', 'ppds.data')
ppds <- if (file.exists(ppds_data)) {
  readRDS(ppds_data)
} else {
  gw_ppd(draws, cache_dir = opts$cache, batch_size = 10)  # calculate once, recycled below!
}

# Y_out_of_sample <- if (length(opts$p_parts) > 1) resp_data else NULL
pred <- gw_vis_fit(ppds, opts, Y = resp_data,
                   n_yhat_draws = 100, cache_dir = opts$cache,
                   file_ext = 'png')
po <- gw_vis_po(ppds, opts, Y = resp_data, cache_dir = opts$cache,
                file_ext = 'png')

dqs_data <- file.path(opts$cache, 'dqs', 'dqs.data')
dqs <- if (file.exists(dqs_data)) {
  readRDS(dqs_data)
} else {
  gw_dqs(draws, other_params = greenwave:::param_names(), cache_dir = opts$cache, n_samples = 250,
         batch_size = 50)
  # gw_dqs(draws, other_params = greenwave:::param_names(), cache_dir = opts$cache)
}

dqs_tidy <- gw_dqs_tidy(dqs)
dq_just_params <- dqs_tidy$fixed_plus_random %>%
  filter(dq %in% c(greenwave:::param_names(), 'alpha2')) %>%
  gw_vis_dqs(cache_dir = opts$cache, suffix = 'just-params', file_ext = 'png', n_cols = 1)

dqs_tidy$fixed_plus_random %>% 
  filter(dq %in% c(greenwave:::param_names(), 'alpha2')) %>% 
  spread(dq, value) %>% 
  # distinct(group_idx, loc_idx, year) %>% arrange(loc_idx, year)
  View()
# filter(alpha2 <= alpha1)

# focal_dqs <- c('sos', 'accum_vi_over_min', 'n_days_over_thresh')
# dq_other_errbars <- dqs_tidy$fixed_plus_random %>%
#   filter(dq %in% focal_dqs) %>% 
#   # filter(!dq %in% c(greenwave:::param_names(), 'alpha2')) %>%
#   gw_vis_dqs(cache_dir = opts$cache, suffix = 'extra', file_ext = 'png', n_cols = 1)
