debug <-interactive()
if (debug) {
  # devtools::install_github("greta-dev/greta")
  # install.packages('abind'); install.packages('moments')
  invisible(sapply(list.files("R", full.names = TRUE), source))
  source('R/utils-debug.R'); pkg_imports()
} else {
  # {  # for no-wireless situations!
    # devtools::build(path = "/tmp", vignettes = FALSE)
    # install.packages("/tmp/greenwave_0.2.0.tar.gz", repos = NULL)
  # }
  # devtools::document(); devtools::install()
  library(greenwave)
  library(dplyr)
}

geo <- 'jornada'  # one of: 'jornada' or 'ikh-nart'
if (interactive()) {
  # opts <- gw_opts(
  #   n_chains = 1, n_burnin = 3000, n_iter = 1000,
  #   # n_attempts = 2, n_extra = 500,
  #   n_years = 6, n_locs = 5,
  #   #excl_params = 'c("omega2")',  # "omega1_R"
  #   cache = 'sandbox/nosim',  # ifelse(debug, "none", "temporary"),
  #   use_single_spacetime = TRUE,
  #   resp_data = "data(csutest_class52_landsat)",  # or a csv: e.g., ikh nart or WDPA 1, 2, ..., N
  #   cov_data = "data(csutest_class52_drivers)",
  #   p_parts = c(training = 0.75, validation = 0.15, test = 0.1), n_ho_locs = 1,
  #   n_aft = 0, n_fore = 1,
  #   #reg_on_sigma = "logit",
  #   sf = list(scale = list(delta1 = 1.5, delta2 = 1.5))
  # )

  opts <- gw_opts(
<<<<<<< HEAD
    n_chains = 3, n_burnin = 3000, n_iter = 1500,
=======
    n_chains = 2, n_burnin = 2000, n_iter = 1000,
>>>>>>> vl/deltas
    # n_attempts = 2, n_extra = 500,
    n_years = 5, n_locs = 3,
    excl_params = 'c("omega2")',  # "omega1_R"
    # excl_re = list(kappa = 'alpha1'),
    cache = ifelse(geo == 'jornada', 'cache/apis-synth-new/rs', 'cache/ikh-nart'),
    use_single_spacetime = TRUE,
<<<<<<< HEAD
    # re_prior_dist = 'cauchy',
    resp_data = 'data-raw/apis/ar/MODIS-multi-loc.csv',  # 'data-raw/trial-vis-seed-1984.csv',  # "data(csutest_class52_landsat)"
    cov_data = 'none',  # 'data-raw/trial-drivers-seed-1984.csv',  # "data(csutest_class52_drivers)"
    p_parts = c(training = 0.9, test = 0.1),
    # Lmin = 10, Lmax = 20, epsilon = 0.005,
    # sf = list(scale = list(gamma2 = 0.08 / 2,
    #                        omega1_R = 0.001 *2)),
    dttm_attr = ifelse(geo == 'jornada', 'img_date', 'date'),
    # reg_on_sigma = "log",
    n_ho_locs = 0, n_aft = 0, n_fore = 0
=======
    resp_data = 'data-raw/trial-vis-seed-1984.csv',  # "data(csutest_class52_landsat)"
    cov_data = 'data-raw/trial-drivers-seed-1984.csv',  # "data(csutest_class52_drivers)"
    p_parts = c(training = 0.75, validation = 0.15, test = 0.1), n_ho_locs = 1,
    n_aft = 0, n_fore = 1  #, reg_on_sigma = "logit"
>>>>>>> vl/deltas
  )
} else {
  opts <- gw_opts()
}
# Rscript demo/gw-nosim.R --n_locs=5 --n_years=5 --n_chains=2 --n_burnin=2500 --n_iter=2500 \
#   --resp_data="data(csutest_class52_landsat)" --cov_data="data(csutest_class52_drivers)" \
#   --cache='sandbox/nosim' --excl_params='omega2' --use_single_spacetime \
#   --p_parts="c(training=0.8,test=0.2)" --n_ho_locs=1 --n_aft=0 --n_fore=0
print(opts)

# ==============================

# Load response and covariate data.
resp_data <- gw_partition_vi(opts$resp_data, opts$cov_data, opts,
                          vi = "NDVI", date = "date", loc_id = "location") %>%
  filter(cal_year < 2020)
cov_data <- if (!is.null(opts$cov_data)) {
  opts$cov_data %>%
    # Apply location filters supplied via opts.
    filter(location %in% unique(resp_data$loc_id)) %>%
    mutate(location = factor(location, levels = levels(resp_data$loc_id)))
} else {
  NULL
}

obs <- gw_vis_obs(resp_data, cache_dir = opts$cache)
obs$obs_type_detailed

Y_in_sample <- resp_data %>% filter(partition == 'training')

if (!is.null(cov_data)) {
  Xs_in_sample <- gw_format_covariates(
    data = cov_data,
    Y = Y_in_sample,  # LJZ was: Y_in_sample
    effects = NULL,
    # effects = list(alpha1 = c('pdsi', 'swe'),
    #                delta1 = c('pdsi'),
    #                delta2 = c('vpd'),
    #                gamma1_R = c('eastness', 'slope'),
    #                gamma2 = c('pr', 'aet')),
    loc_id = "location",
    cal_year = "year",
    to_z_score = TRUE
  )
} else {
  Xs_in_sample <- NULL
}

draws <- gw_fit(Y_in_sample, Xs_in_sample, opts, pooled = T)  #

diagnostics<- gw_diagnostics(draws, cache_dir = opts$cache)
ppcs <- gw_vis_ppcs(draws, cache_dir = opts$cache)

# traces <-
gw_vis_traces(draws, coefs = 'random')  # cache_dir = opts$cache

# pred <- gw_vis_fit(draws, opts, n_yhat_draws = 100, cache_dir = opts$cache,
#                    Y = resp_data)

ppds <- gw_ppd(draws)  # calculate once, recycled below!
pred <- gw_vis_fit(ppds, opts, n_yhat_draws = 100, cache_dir = opts$cache,
                   Y = resp_data)
po <- gw_vis_po(ppds, opts, n_yhat_draws = 100, cache_dir = opts$cache,
                Y = resp_data)

sf_eval <- gw_param_scaled_dists(draws, opts$cache, facet_scales = 'free')
# y_from_priors <- calculate(get('y', attr(draws, 'fit_info')), nsim = 100)  # does not work


ppcs <- gw_ppc(draws, cache_dir = opts$cache)

diagnostics <- gw_diagnostics(draws, cache_dir = opts$cache) #%>%
  # left_join(attr(sim, "fixed_effect_type"))
# print(diagnostics, n = Inf)

benchmark <- gw_benchmark(draws, cache_dir = opts$cache)

# 'level 1' traces
traces <- gw_trace(draws,
  cache_dir = opts$cache, annotation = TRUE, diagnostics = TRUE)

# level 2 traces (SLOW!)  # try trace_batch_size 100 vs Inf
# system.time({
  traces_re <- gw_trace(draws,
                        random_effects = TRUE, cache_dir = opts$cache,
                        annotation = TRUE, diagnostics = FALSE)
# })


# ppds <- gw_ppd(draws, n_samples = n_sim(draws, n_samples))
  system.time({
    pred <- gw_vis_fit(draws, opts, n_yhat_draws = 100, cache_dir = opts$cache,
                       Y = resp_data)
  })


# po <- gw_vis_po(draws, opts, n_yhat_draws = 100, cache_dir = opts$cache,
#           Y = resp_data)
>>>>>>> vl/deltas

# Derived quantities stuff.
add_params <- NULL  # c('gamma1_R', 'gamma2', 'omega1_R')
dqs <- gw_dqs(draws, what = 'fixed_plus_random', other_params = add_params)
dqs_tidy <- gw_dqs_tidy(dqs)


timing <- dqs_tidy$fixed_plus_random %>%
  filter(!dq %in% c('alpha1', 'alpha2', add_params)) %>%
  gw_vis_dqs(opts$cache, lab_width = 16)  # facet_scales = 'fixed'
timing

message(sprintf("See %s for details!", opts$cache))
