source('R/dacqre.R')

driver_info <- make_dacqre('data-raw/jornada/dacqre')
driver_info_long <- driver_info %>%
  gather(driver, value, -location, -year) %>%
  mutate(driver_class = ifelse(driver %in% names(geophysical_data),
                               'geophysical', 'climate / weather')) %>%
  left_join(distinct(resp_data, loc_id, loc_idx, cal_year),
            by = c('location' = 'loc_id', 'year' = 'cal_year')) %>%
  filter(!is.na(loc_idx))

# ggplot(driver_info_long %>% filter(driver_class != 'geophysical')) +
#   facet_grid(driver ~ ., scales = 'free') +
#   geom_point(aes(x = year, y = value), alpha = 0.1)

dq_summary <- gw_summarize_tidy_dqs(dqs_tidy$fixed_plus_random, ci_mass = 0.89)

# resp_and_drivers_data <-  %>%
#   left_join(driver_info_long, by = c('loc_id' = 'location', 'cal_year' = 'year'))

driver_info_long

# unique(dq_summary$dq)

tmp <- dq_summary %>%
  # filter(dq == 'alpha1') %>%
  left_join(driver_info_long,
            by = c('loc_idx', 'cal_year' = 'year')) %>%
  filter(driver_class != 'geophysical')
tmp

# tmp %>%
#   select(loc_idx, cal_year, hdi_med, matches(get_met_vars('DAYMET'))) %>%
#   gather()

ggplot(tmp) +
  facet_grid(dq~driver, scales = 'free') +
  geom_point(aes(x = value, y = hdi_med), alpha = 0.5)
