#' Format covariate data
#'
#' Prepare a list of covariate matrices for fixed effects in parameter models to
#' use in [gw_fit()].
#'
#' @param data Space- and/or time-varying covariate data. In addition to storing
#' individual covariates in separate columns, the data *must* include attributes
#' for year and location.
#' @param Y Output from [gw_format_vi()].
#' @param loc_id The column containing the ID for the location associated
#' with each observation.
#' @param cal_year The column containing the calendar year associated with a
#' given covariate value.
#' @param effects List of character vectors of covariates to include as fixed
#' effects in each parameter model. The names of the list elements must
#' correspond to the model parameters, any of: "alpha1", "lambda", "delta1",
#' "delta2", "gamma1_R", "gamma2", "omega1_R", "omega2". The list requires
#' entries only for those parameters for which you want fixed effects.
#' @param end_year Final covariate year to consider. Must be greater than or
#' equal to the maximum year for which there are data in Y. If end_year is
#' greater than the maximum year in Y, then predictions (i.e., forecasts) can be
#' made for these years despite there being no observations.
#' @param to_z_score Scale the covariates?
#'
#' @return A list of covariate matrices in format suitable for [gw_fit()].
#'
#' @importFrom dplyr arrange distinct filter group_indices left_join mutate mutate_all mutate_at n n_distinct select vars
#' @importFrom lubridate year
#' @importFrom rlang .data as_string ensym
#' @importFrom tibble enframe
#' @importFrom tidyr gather
#' @importFrom tidyselect all_of any_of matches
#'
#' @examples
#' library(dplyr)
#' library(lubridate)
#' data("csutest_class52_landsat", "csutest_class52_drivers")
#'
#' Y <- csutest_class52_landsat %>%
#'   # filter years to make fit less intensive
#'   filter(year(date) > 2013, year(date) < 2017, location != "csutest_46") %>%
#'   gw_format_vi(vi = "NDVI", date = "date", loc_id = "location")
#'
#' effects <- list(alpha1 = 'year', gamma1_R = 'elevation')
#'
#' Xs <- gw_format_covariates(
#'   data = csutest_class52_drivers,
#'   Y = Y,
#'   effects = effects,
#'   loc_id = "location",
#'   cal_year = "year",
#'   to_z_score = TRUE
#' )

#' @rdname gw-format-covariates
#' @export
gw_format_covariates <- function(data, Y,
                                 loc_id = "loc_id", cal_year = "cal_year",
                                 effects = NULL, end_year = NULL,
                                 to_z_score = FALSE) {

  loc_id_var <- as_string(ensym(loc_id))
  data$cal_year <- data[[as_string(ensym(cal_year))]]

  if (max(data$cal_year) < max(Y$cal_year)) {
    # TODO: we might make this a bit more sophisticated to include messages
    # about the number of year affected, which years, etc.
    stop("You are missing covariate data for one or more years!")
  }

  start_year <- min(Y$cal_year) - 1 - (min(Y$year) - 1) # the extra minus one is to
  # get one year earlier for omega1_R and gamma1_R
  if (is.null(end_year)) {
    end_year <- max(Y$cal_year)
  }

  if (end_year < max(Y$cal_year)) {
    stop("end_year must be greater than or equal to the latest year in Y")
  }

  if (is.null(effects)) {
    data <- data %>%
      select(c(cal_year, loc_id = all_of(loc_id_var)))
  } else {
    data <- data %>%
      select(c(cal_year, loc_id = all_of(loc_id_var)),
             any_of(unique(unlist(effects))))
  }

  data <- data %>%
    filter(cal_year >= start_year, cal_year <= end_year,
           loc_id %in% Y$loc_id) %>%
    # Join over location indices from Y to ensure match up
    left_join(distinct(Y %>% select(.data$loc_id, .data$loc_idx)),
              by = "loc_id") %>%
    # Arrange covariates to fit order of Xs needed for gw_fit
    arrange(.data$cal_year, .data$loc_idx)

  if (n_distinct(data$cal_year) != end_year - start_year + 1) {
    stop("You are missing covariate data for one or more years")
  }

  # Add new location indices to observations within the covariates data for
  # which there are no corresponding observations in the response data. Because
  # these locations are not present in Y, they require novel indices.
  if (any(is.na(data$loc_idx))) {
    data$loc_idx[is.na(data$loc_idx)] <-
      # group_indices(filter(data, is.na(data$loc_idx)), loc_id) +
      as.integer(as.factor(filter(data, is.na(data$loc_idx)), loc_id)) +
      max(data$loc_idx, na.rm = TRUE)
  }


  J <- n_distinct(data$loc_idx)
  K <- max(Y$year) + (end_year - max(Y$cal_year))  # this needs work. how does
  # adding prior years work?? also this may need to be defined by data not Y

  # Create intercept matrices for each param
  Xs <- replicate(length(param_names()), matrix(1, J * K), simplify = FALSE)
  names(Xs) <- param_names()
  Xs$gamma1_R <- matrix(1, J * (K + 1))
  Xs$omega1_R <- matrix(1, J * (K + 1))

  for(name in names(effects)) {
    covar_names <- effects[[name]]
    covar_sub <- data %>% select(.data$cal_year, all_of(covar_names))
    if (!(name %in% c("gamma1_R", "omega1_R"))) {
      covar_sub <- covar_sub %>% filter(.data$cal_year > start_year)
    }
    Xs_tbl <- covar_sub %>%
      select(-.data$cal_year) %>%
      mutate_all(scale_covariate, do_scaling = to_z_score)
    Xs[[name]] <- cbind(Xs[[name]], as.matrix(Xs_tbl))
  }

  # TODO: create attribute for type of fixed effect
  # attr(Xs, "fixed_effect_type") <- bind_rows(fe_info)
  # c('x_space', 'x_time', 'x_spacetime')
  Xs

}

scale_covariate <- function(x, do_scaling) {
  if (do_scaling) {
    scale(x)[, 1]
  } else
    x
}

get_int_only_Xs <- function(Y) {

  end_year <- max(Y$cal_year)

  J <- n_distinct(Y$loc_idx)
  K <- max(Y$year) + (end_year - max(Y$cal_year))

  Xs <- replicate(length(param_names()), matrix(1, J * K), simplify = FALSE)
  names(Xs) <- param_names()
  Xs$gamma1_R <- matrix(1, J * (K + 1))
  Xs$omega1_R <- matrix(1, J * (K + 1))

  Xs[sort(names(Xs))]

}

get_Xs_tilde <- function(Xs, scenario_vars) {
  
  Xs_tilde <- lapply(Xs, function(X) {
    
    x_raw <- X[, dimnames(X)[[2]] %in% scenario_vars, drop = FALSE]
    x_raw_split <- split(x_raw, col(x_raw, as.factor = TRUE))
    lapply(x_raw_split, function(x) c(min = min(x), mean = 0, max = max(x)))
    
  })

  scenarios_list <- flatten(unique(Xs_tilde))
  uniq_scenarios <- scenarios_list[!duplicated(scenarios_list)]

  d <- do.call(expand.grid, uniq_scenarios) %>% 
    mutate(loc_idx = 1:n())
  n_scenarios <- max(d$loc_idx)
  d_lkp <- data.frame(loc_idx = rep(1:n_scenarios, times = 3 +1), 
                      year = rep(0:3, each = n_scenarios))
  scenarios <- d_lkp %>% left_join(d, by = "loc_idx")
  
  get_scenario_desc <- function(x) {
    z <- ifelse(x == min(x), 'min', ifelse(x == max(x), 'max', 'mean'))
    factor(z, levels = c('min', 'mean', 'max'))
  }
  scenario_metadata <- d %>% 
    mutate_at(vars(-.data$loc_idx), list(desc = ~ get_scenario_desc(.)))
  
  Xs_tilde <- lapply(names(Xs), function(w) {
    
    X <- Xs[[w]] 
    N <- ifelse(grepl('_R', w),  n_scenarios * (3 + 1),  n_scenarios * 3)
    
    X_tilde <- matrix(NA, nrow = N, ncol = ncol(X))
    X_tilde[, 1] <- 1
    X_tilde[, -1] <- 0
    dimnames(X_tilde) <- dimnames(X)
    
    for(colname in dimnames(X_tilde)[[2]]) {
      if (colname %in% scenario_vars) X_tilde[, colname] <- 
          tail(scenarios[[colname]], N)
    }
    X_tilde
    
  })
  names(Xs_tilde) <- names(Xs)
  
  attr(Xs_tilde, 'scenario_info') <- 
    rlang::env(J = n_scenarios, K = 3, scenarios = scenarios,
               scenario_metadata = scenario_metadata) 
  Xs_tilde
  
}
