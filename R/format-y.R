#' Format time series of vegetation indices
#'
#' Prepare long-form data containing the response information (time series of
#' vegetation indices) for the greenwave model (i.e., [gw_fit()]). This function
#' assumes any annd all QA / QC filtering has been done previously.
#'
#' @param data Data frame consisting of time series vegetation index as well as
#' location and year information.
#' @param vi The column containing the response data (e.g., NDVI, EVI, or GCC
#' observations).
#' @param date The column with the acquisition date corresponding to each
#' observation.
#' @param loc_id The column containing the ID for the location associated
#' with each observation.
#' @param left_pad Postitive integer, default 0. Number of years to pad before
#' the earliest year for which there are observations. E.g., set `left_pad = 1`
#' to hindcast one year. Note that you will need to provide covariates for each
#' hindcasted year if you are modeling fixed effects.
#' @param other_keepers Other columns to keep. Optional.
#' @param cache_dir Optional directory to which outputs should be written. A
#' character. Defaults to `NULL`.
#'
#' @return A data frame ready for use by [gw_fit()] with the following columns:
#' * `vi`: vegetation index value
#' * `cal_year`: the calendar year
#' * `year`: year index (after taking into account any padding for hindcasting)
#' * `loc_id`: the plain text name of the location
#' * `loc_idx`: location index
#' * `day`: day index for entire time series, where day = 1 corresponds January
#' 1st of the earliest year for which you have observations minus `left_pad`
#' * `doy`: the calendar day of year for the observation
#' * `obs_type`: estimation, interpolation, or extrapolation
#' * `partition`: training, validation, or test set
#'
#' @importFrom dplyr arrange filter group_indices mutate select
#' @importFrom lubridate floor_date yday year years ymd
#' @importFrom rlang .data as_string ensym
#' @importFrom tibble add_column tibble
#' @importFrom tidyselect all_of
#' @importFrom tidyr complete nesting
#'
#' @examples
#' library(dplyr)
#' library(lubridate)
#' data("csutest_class52_landsat")
#'
#' Y <- csutest_class52_landsat %>%
#'   # filter years to make fit less intensive
#'   filter(year(date) > 2013, year(date) < 2017, location != "csutest_46") %>%
#'   gw_format_vi(vi = "NDVI", date = "date", loc_id = "location")

#' @rdname gw-format-vi
#' @export
gw_format_vi <- function(data,
                         vi = "vi",
                         date = "date",
                         loc_id = "loc_id",
                         left_pad = 0,
                         other_keepers = NULL,
                         cache_dir = NULL) {

  vi_var <- as_string(ensym(vi))
  date_var <- as_string(ensym(date))
  loc_id_var <- as_string(ensym(loc_id))

  type_defaults <- tibble(obs_type = '.estim', partition = 'training')

  out <- data %>%
    select(c(vi = all_of(vi_var),
             date = all_of(date_var), loc_id = all_of(loc_id_var),
             all_of(other_keepers))) %>%
    arrange(.data$date) %>%
    # mutate(cal_year = year(.data$date),
    #        loc_idx = group_indices(., .data$loc_id)) %>%
    mutate(cal_year = year(.data$date),
           loc_idx = as.integer(as.factor(.data$loc_id))) %>%
    complete(cal_year = seq.int(min(.data$cal_year), max(.data$cal_year)),
             nesting(loc_id, loc_idx)) %>%
    # mutate(year = group_indices(., .data$cal_year)) %>%
    mutate(year = as.integer(as.factor(.data$cal_year))) %>%
    filter(!is.na(vi)) %>%
    mutate(doy = yday(.data$date),
           day = as.numeric(.data$date
                            - (ymd(floor_date(min(.data$date), unit = "years"))
                               - years(left_pad))) + 1) %>%
    mutate(year = .data$year + left_pad) %>%
    select(c(.data$vi, .data$cal_year, .data$year, .data$loc_id, .data$loc_idx,
             .data$day, .data$doy, all_of(other_keepers))) %>%
    # If obs_type doesn't already exist, add it.
    add_column(!!!type_defaults[setdiff(names(type_defaults), names(.))])

  save_table(out, dir(cache_dir, 'data'), "Y-all-parts.csv")

}

#' @rdname gw-format-vi
#' @export
gw_partition_vi <- function(Y, Xs = NULL, opts, ..., other_keepers = NULL) {

  set.seed(opts$seed)

  dttm_attr <- opts$dttm_attr
  date_var <- ensym(dttm_attr)

  loc_attr <- opts$loc_attr
  loc_id_var <- ensym(loc_attr)

  # Define in- and out-of-sample locations.
  if (opts$n_locs == 1 | n_distinct(Y[[loc_id_var]]) == 1) {
    all_locs <- unique(Y[[loc_id_var]])
    in_sample_locs <- all_locs
  } else {
    all_locs <- sample(unique(Y[[loc_id_var]]), opts$n_locs + opts$n_ho_locs)
    in_sample_locs <- sort(sample(all_locs, opts$n_locs))
  }
 

  diff_locs <- setdiff(all_locs, in_sample_locs)
  out_of_sample_locs <- if (length(diff_locs) > 0) sort(diff_locs) else NULL

  parts <- enframe(opts$p_parts, 'part', 'frac')
  parts_notrain <- parts %>%
    filter(.data$part != 'training') %>%
    mutate(rel_frac = .data$frac / sum(.data$frac))

  Y_max_year <- max(year(Y[[date_var]]))
  Xs_max_year <- if (is.null(Xs)) Y_max_year else max(Xs$year)
# browser()
  out <- Y %>%
    filter(year(!!date_var) <= Xs_max_year,  # observations require covariates
           !!loc_id_var %in% all_locs) %>%
    filter(year(!!date_var) > max(year(!!date_var)) -
             opts$n_fore - opts$n_years - opts$n_aft) %>%
    # If there are holdout locations, ensure the holdouts appear first at j = J + 1.
    mutate(!!loc_id_var := factor(!!loc_id_var,
                             levels = c(in_sample_locs, out_of_sample_locs)),
           obs_idx = 1:n()) %>%
    mutate(partition = sample(parts$part, n(), replace = TRUE, prob = parts$frac),
           # Spatial, temporal, and spatiotemporal extrapolation.
           .extrap_j = !(!!loc_id_var %in% in_sample_locs),
           .extrap_k = !((year(!!date_var) <= max(year(!!date_var)) - opts$n_fore) &
                           (year(!!date_var) > max(year(!!date_var)) - (opts$n_fore + opts$n_years))),
           .extrap_jk = .data$.extrap_j & .data$.extrap_k,
           # Interpolation.
           .interp = .data$partition != "training" & !.data$.extrap_j & !.data$.extrap_k,
           # Estimation.
           .estim = .data$partition == "training" & !.data$.extrap_j & !.data$.extrap_k) %>%
    mutate_at(vars(c('.extrap_j', '.extrap_k')),
              list(function(x, is_both) ifelse(is_both, FALSE, x)),
              is_both = .$.extrap_jk) %>%
    # Consolidate columns above into a single 'type' attribute.
    pivot_longer(cols = -matches('^[^\\.]'), 
                 names_to = 'obs_type', values_to = 'obs_is_type') %>%
    filter(.data$obs_is_type) %>% arrange(.data$obs_idx) %>% select(-.data$obs_is_type) %>%
    mutate(partition = ifelse(.data$obs_type != '.estim' & n_distinct(.data$partition) > 1,
                              sample(parts_notrain$part, n(), prob = parts_notrain$rel_frac,
                                     replace = TRUE),
                              "training")) %>%
    # Randomly drop some loc-years?
    group_by(year(!!date_var), !!loc_id_var) %>% 
    mutate(ho_jk = rbernoulli(1, as.integer(.data$obs_type == '.estim') * opts$p_ho_jk),
           obs_type = ifelse(ho_jk, '.interp', obs_type),
           partition = ifelse(ho_jk, 
                              sample(parts_notrain$part, n(), prob = parts_notrain$rel_frac,
                                     replace = TRUE), 
                              partition)) %>% 
    ungroup() %>% 
    gw_format_vi(..., date = !!date_var, loc_id = !!loc_id_var, 
                 other_keepers = c('obs_type', 'partition', other_keepers))
  
  save_table(out, dir(opts$cache, 'data'), "Y-all-parts.csv")

}
