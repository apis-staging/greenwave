gw_driver_coefs <- function(draws, effects, cache_dir = NULL, file_ext = 'pdf') {
  coef_pattern <- sprintf('beta_%s\\[[2-9],1\\]', greenwave:::param_names())
  
  param_to_coef_plotmath <- enframe(effects) %>% 
    unnest_longer(value) %>% 
    mutate(param = grep(paste(coef_pattern, collapse = '|'), 
                        dimnames(draws[[1]])[[2]], value = TRUE),
           param_plotmath = parse_param(param)$param_expr,
           coef_plotmath = sprintf('%s: %s', param_plotmath, value))
  coef_plotmath <- param_to_coef_plotmath$coef_plotmath
  names(coef_plotmath) <- param_to_coef_plotmath$param
  
  my_labeller_one <- as_labeller(
    x = coef_plotmath, 
    default = label_parsed
  )
  
  set_theme()
  p <- bayesplot::mcmc_hist(draws, regex_pars = coef_pattern, 
                       facet_args = list(ncol = 1, labeller = my_labeller_one)) +
    geom_vline(xintercept = 0, linetype = 'dashed')
  save_figure(p, dir(cache_dir, 'drivers'), 
              sprintf('driver-coef-hists.%s', file_ext),
              p_width = 16*0.25, p_height = 9*0.25)
}
