#' Plotting utilities for derived quantities
#'
#' Various derived quantities visualization. (A work in progress.)
#'
#' @param draws Posterior samples from the sampling phase. An MCMC list object.
#' @param ... Optional args to be passed to [gw_summarize_tidy_dqs()].
#' @param cache_dir Optional directory to which outputs should be written. A
#' character. Defaults to `NULL`.
#' @param lab_width Maximum number of characters before wrapping strip text.
#' @param facet_scales One of 'free' or 'fixed'.
#' @param n_cols Number of columns over which the sequence of panels will be
#' wrapped.
#' @param ci_mass The mass &ndash; a scalar \[0, 1\] &ndash; used to compute
#' credible intervals (i.e., highest density intervals).
#' @param metadata Optional additional dataset for plotting / filtering. This
#' is currently just a placeholder for functionality to be implemented in the
#' future.
#' @param suffix Optional suffix for the resulting figure.
#' @param file_ext File extension.
#' @param data Derived quantities data frame.
#'
#' @importFrom dplyr filter group_by left_join mutate n_distinct select summarise ungroup
#' @importFrom ggplot2 aes element_text facet_grid geom_label geom_line geom_point geom_vline ggplot labs scale_color_brewer scale_y_reverse theme geom_errorbar label_parsed lims position_dodge scale_alpha_identity scale_color_identity scale_fill_gradientn scale_linetype_identity scale_x_discrete
#' @importFrom ggridges geom_density_ridges
#' @importFrom HDInterval hdi
#' @importFrom rlang .data
#' @importFrom scales brewer_pal pretty_breaks
#' @importFrom stats median
#' @importFrom tidyr spread

#' @rdname gw-vis-dqs
#' @export
gw_vis_dqs <- function(draws, ..., cache_dir = NULL, lab_width = 16,
                       facet_scales = 'free', n_cols = 3, ci_mass = 0.89,
                       metadata = NULL, suffix = NULL, file_ext = 'pdf') {

  set_theme()

  dq_summary <- gw_summarize_tidy_dqs(draws, ..., ci_mass = ci_mass)

  n_locs <- n_distinct(draws$loc_idx)
  pos <- if (n_locs > 1) {
    position_dodge(width = 0.3)
  } else {
    position_dodge(width = 0)
  }

  n_locs <- n_distinct(dq_summary$loc_idx)
  n_years <- n_distinct(dq_summary$cal_year)

  if (!is.null(metadata)) {
    dq_summary <- dq_summary %>% left_join(metadata)
  }

  p <- ggplot(dq_summary %>% filter(.data$loc_idx <= 8),
              aes(x = factor(.data$cal_year), group = .data$loc_idx,
                  color = factor(.data$loc_idx))) +
    facet_wrap( ~ dq_lab, scales = facet_scales, ncol = n_cols,
               labeller = label_parsed) +  # label_wrap_gen(width = lab_width)
    geom_line(aes(y = hdi_med),
              linetype = 'dashed', alpha= 0.5, position = pos) +
    # geom_errorbar(aes(ymin = .data$hdi_min, ymax = .data$hdi_max),
    #               width = 0, size = 1.25 / n_locs, position = pos) +
    geom_errorbar(aes(ymin = .data$hdi_25, ymax = .data$hdi_75),
                  width = 0, size = 3 / n_locs, position = pos) +
    geom_point(aes(y = .data$hdi_med),
               pch = 21, fill = 'white', size = 2.5, position = pos, stroke = 1.25) +
    scale_color_brewer('Location', type = 'qual', palette = 'Set2') +
    labs(x = 'Year', y = '') +
    scale_x_discrete(breaks = pretty_breaks(n = floor(n_years / 3)))

  save_figure(p, dir(cache_dir, 'dqs', 'whisker-plots'),
              sprintf('%s.%s', paste(c('dqs', suffix), collapse = '-'), file_ext),
              p_width = n_distinct(draws$year) * 0.75, p_height = 1.75)
}

#' @rdname gw-vis-dqs
#' @export
gw_vis_departures <- function(data) {  # call: gw_vis_departures(bind_rows(gs_timing))
  browser()
  # gs_departure_summary <-
  data %>%
    pivot_wider(names_from = .data$dq, values_from = .data$value) %>%
    select(.data$sim_idx, .data$loc_idx, .data$year, .data$effect,
           .data$`eos - sos`) %>%
    pivot_wider(names_from = .data$effect, values_from = .data$`eos - sos`)
    # spread(.data$effect, .data$`eos - sos`) %>%
    group_by(.data$loc_idx, .data$year) %>%
    summarise(departure = mean(.data$fixed_plus_random - .data$fixed)) %>%
    ungroup()

  tmp <- data %>%
    left_join(gs_departure_summary) %>%
    filter(quantity %in% c('eos - sos')) %>%
    mutate(fill_col = ifelse(.data$effect == 'fixed', NA, .data$departure),
           line_col = ifelse(.data$effect == 'fixed', 'gray50', 'black'),
           grp_alpha = ifelse(.data$effect == 'fixed', 0.2, 0.8),
           grp_lty = ifelse(.data$effect == 'fixed', 'dashed', 'solid'))

  spectral <- brewer_pal(type = 'div', palette = 'Spectral', direction = 1)(11)
  limit <- max(abs(gs_departure_summary$departure)) * c(-1, 1)
  grand_median <- gs_timing$fixed %>%
    group_by(.data$quantity, .data$loc_idx) %>%
    summarise(fixed_median = median(.data$value)) %>%
    ungroup() %>%
    filter(quantity == 'eos - sos')
  ggplot(tmp) +
    facet_grid(quantity ~ loc_idx) +
    geom_vline(data = grand_median, aes(xintercept = .data$fixed_median),
               linetype = 'dotted') +
    geom_density_ridges(aes(x = .data$value, y = .data$year,
                            group = interaction(.data$effect, .data$year),
                            fill = .data$fill_col, alpha = .data$grp_alpha,
                            linetype = .data$grp_lty, color = .data$line_col),
                        bandwidth = 5, scale = 0.95) +
    geom_label(data = grand_median,
               aes(x = .data$fixed_median,  y = Inf,
                   label = round(.data$fixed_median, 2)),
               vjust = 'inward', fill = 'white') +
    scale_y_reverse() +
    labs(x = "Value", y = "Year") +
    scale_alpha_identity() + scale_linetype_identity() + scale_color_identity() +
    theme(axis.title.y = element_text(angle = 90)) +
    scale_fill_gradientn('Departure', colors = spectral, limit = limit) +
    lims(x = hdi(tmp$value, credMass = 0.975))
}
