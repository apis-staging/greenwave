#' Benchmarking
#'
#' System info, time per iteration, etc.
#'
#' @param draws Posterior samples from the sampling phase. An MCMC list object.
#' @param cache_dir Optional directory to which outputs should be written. A
#' character. Defaults to `NULL`.
#'
#' @importFrom dplyr bind_cols tibble
#'
#' @keywords internal

benchmark <- function(draws, cache_dir = NULL) {
  save_table(bind_cols(get_sys_info(), get_speed(draws)),
             cache_dir, 'benchmark.csv')
}

get_sys_info <- function() {
  bind_cols(get_cpu_info(), get_gpu_info())
}

get_speed <- function(draws) {
  opts <- get('opts', attr(draws, 'fit_info'))
  n_iter_combined <- opts$n_burnin + opts$n_iter
  time_elapsed <- get('time_elapsed_total', attr(draws, 'fit_info'))
  tibble(minutes_for_1K_iters = time_elapsed / n_iter_combined * 1000)
}

get_cpu_info <- function() {

  vendor_name <- system("cat /proc/cpuinfo | grep 'vendor' | uniq | sed 's/^.*: //'",
                        intern = TRUE)
  model_name <- system("cat /proc/cpuinfo | grep 'model name' | uniq | sed 's/^.*: //'",
                       intern = TRUE)
  n_processors <- system("cat /proc/cpuinfo | grep processor | wc -l", intern = TRUE)
  n_cores <- system("cat /proc/cpuinfo | grep 'core id' | sort --unique | wc -l",
                    intern = TRUE)

  data.frame(vendor_name = vendor_name,
             model_name = model_name,
             n_processors = n_processors,
             n_cores = n_cores)

}

get_gpu_info <- function() {

  suppressWarnings({
    cuda_version <- system("srun nvidia-smi -q 2> /dev/null | grep 'CUDA Version' | sed 's/^.*: //'",
                           intern = TRUE)
    driver_version <- system("srun nvidia-smi -q 2> /dev/null | grep 'Driver Version' | sed 's/^.*: //'",
                             intern = TRUE)
    product_name <- system("srun nvidia-smi -q 2> /dev/null | grep 'Product Name' | uniq | sed 's/^.*: //'",
                           intern = TRUE)
    device_memory <- system("srun nvidia-smi -q 2> /dev/null | awk '/FB Memory Usage/,/Total/' | sort --unique | grep -E -o '[0-9]+'",
                            intern = TRUE)
  })

  gpu_info <- data.frame(cuda_version = cuda_version,
             driver_version = driver_version,
             product_name = product_name,
             device_memory = device_memory)
  if (nrow(gpu_info) == 0) NULL else gpu_info

}
