make_names <- function(names) {
  paste0('x', make.unique(names, sep = ""))
}
# as_tibble(Xs[[1]], .name_repair = make_names)

to_tbl <- function(X, n_elements) {
  if (ncol(X) == 1) return(NULL)
  as_tibble(X, .name_repair = make_names) %>%
    select(-1) %>%
    tail(n_elements) %>%
    mutate(group_idx = 1:n())
}

get_Xs_long <- function(Xs, envir) {

  fit_info <- mget(c('J', 'K', 'cal_years'), envir)

  all <- all_loc_yr_doy_cmbns(fit_info$J, fit_info$K, fit_info$cal_years)
  locyear_lookup <- lookup_group(all)

  Xs_tbl_raw <- lapply(Xs, to_tbl, n_elements = fit_info$J * fit_info$K)
  bind_rows(Xs_tbl_raw, .id = 'param') %>%
    pivot_longer(cols = -all_of(c("param", "group_idx")),
                 names_to = 'covariate', values_to = 'x') %>%
    left_join(locyear_lookup, by = "group_idx")

}

gw_vis_X <- function(Xs, envir) {

  Xs_tbl <- get_Xs_long(Xs, envir)

  ggplot(Xs_tbl) +
    facet_grid(covariate~param) +
    geom_hline(aes(yintercept = 0), linetype = 'dashed', alpha = 0.5) +
    geom_line(aes(x = cal_year, y = x, group = loc_idx, color = factor(loc_idx)),
              size = 0.8) +
    scale_color_brewer('Location', type = 'qual', palette = 'Set2') +
    labs(x = 'Year', y = 'Covariate value (standardized)') +
    theme(axis.title.y = element_text(angle = 90))

}

gw_dqs_X <- function(draws, Xs,
                     n_samples = NULL, other_params = NULL, cache_dir = NULL,
                     suffix = 'scenarios') {
  
  fit_info <- mget(c('params', 'opts'), attr(draws, "fit_info"))
  scenario_info <- mget(c('J', 'K', 'scenarios'), attr(Xs, "scenario_info"))
  
  out <- list()
  
  params_select <- unique(c('alpha1', 'delta1', 'lambda', 'delta2',  # default
                            other_params))
  scenario_promise <- lapply(setNames(params_select, params_select), function(w) {
    
    w_params <- fit_info$params[[w]]
    
    Js <- scenario_info$J
    Ks <- ifelse(grepl('_R', w), scenario_info$K + 1, scenario_info$K)
    
    w_params$phi <- as_data(rep(0, Js))
    w_params$theta <- as_data(rep(0, Ks))
    w_params$kappa <- as_data(rep(0, Js * Ks))
    
    list(linear_cmbn(Xs[[w]], w_params), w_params)
    # okthen = calculate(me, values = draws, nsim = 100)
    
  })
  
  lp <- lapply(scenario_promise, `[[`, 1)
  params <- lapply(scenario_promise, `[[`, 2)
  
  d1 <- 1 / lp$delta1  # greenup duration
  d2 <- 1 / -lp$delta2  # greendown duration (delta2 is negative on its native scale)
  
  alpha2 <- lp$alpha1 + exp(lp$lambda)  # new
  sos <- lp$alpha1 - 0.5 * d1
  eos <- alpha2 + 0.5 * d2
  
  attr(Xs, 'scenario_params') <- params
  yhat <- gw_ppd_tilde(draws, Xs = Xs, what = 'yhat', wait = TRUE)$yhat
  # yhat_test = calculate(yhat, values = draws, nsim = n_sim(draws, n_samples))
  
  default_dqs <- c('alpha1 = lp$alpha1', 'lambda = lp$lambda', 'd1', 'd2',
                   'sos', 'eos', 'eos - sos', 'alpha2')
  other_param_dqs <- if (!is.null(other_params)) {
    excluded_params <- fit_info$opts$exclude$params
    if (!is.null(excluded_params)) {
      other_params <- other_params[!other_params %in% excluded_params]
    }
    sprintf('%s = lp$%s', other_params, other_params)
  } else {
    NULL
  }
  supp_dqs <- 'yhat = yhat' # if (which_effect == 'fixed') NULL else 'yhat = yhat'
  expr <- sprintf("calculate(%s, values = draws, nsim = n_sim(draws, n_samples))",
                  paste(unique(c(default_dqs, other_param_dqs, supp_dqs)),
                        collapse = ', '))
  # browser()
  out$scenarios <- eval(parse(text = expr))
  
  e <- new.env()
  for(n in c('J', 'K', 'scenarios')) {
    assign(n, get(n, attr(Xs, "scenario_info")), e)
  }
  attr(out, "scenario_info") <- e

  save_object(out, dir(cache_dir, 'dqs'),
              sprintf('%s.%s', paste(c('dqs', suffix), collapse = '-'), "data"))
  
}
