#' Plotting utilities
#'
#' Various internal utility functions for plotting, labeling, etc. `set_theme()`
#' sets elements of the active theme for plots produced using \pkg{bayesplot}
#' and \pkg{ggplot2} plotting functions.
#'
#' @importFrom bayesplot bayesplot_theme_set color_scheme_set
#' @importFrom dplyr arrange distinct mutate n pull
#' @importFrom ggplot2 element_rect element_text theme theme_grey theme_set
#' @importFrom ggthemes theme_hc
#' @importFrom grDevices rgb
#' @importFrom grid unit
#' @importFrom scales brewer_pal
#' @importFrom stringr str_to_title
#' @importFrom tidyr unnest_wider
#'
#' @keywords internal

set_theme <- function(font_size = 16, font_family = "serif") {

  bayesplot_theme_set(
    theme_grey() +
      theme_hc(base_size = font_size, base_family = font_family)
  )
  color_scheme_set("brewer-Accent")  # or "mix-blue-red"

  theme_set(
    theme_grey() +
      theme_hc(base_size = font_size, base_family = font_family) +
      theme(strip.text = element_text(size = font_size, color = "white"),
            axis.title.y = element_text(angle = 0, vjust = 0.5),
            legend.key.width = unit(0.06, "npc"),
            legend.key.height = unit(0.025, "npc"),
            panel.spacing = unit(1.1, "lines"),
            strip.background = element_rect(fill = "gray15"))
  )

}

sig_fig <- function(x, k) trimws(format(round(x, k), nsmall = k))

format_and_filter <- function(data, j = NULL, k = NULL) {
  if (is.null(j)) j <- seq.int(n_distinct(data$loc_idx))
  if (is.null(k)) k <- seq.int(n_distinct(data$year))
  grp_math(data) %>%
    filter(.data$loc_idx %in% j, .data$year %in% k)
}

grp_math <- function(data) {
  data %>%
    mutate(loc_math = paste('italic(j)', .data$loc_idx, sep = '=='),
           year_math = paste('italic(k)', .data$year, sep = '=='))
}

obs_type_general <- function() {
  class_pal <- brewer_pal(type = 'qual', palette = 'Paired')(3)
  enframe(list(.estim = c(lab = 'Estimation', col = class_pal[1]),
               .interp = c(lab = 'Interpolation', col = class_pal[2]),
               .extrap_j = c(lab = 'Extrapolation', col = class_pal[3]),
               .extrap_k = c(lab = 'Extrapolation', col = class_pal[3]),
               .extrap_jk = c(lab = 'Extrapolation', col = class_pal[3])),
          name = 'obs_type', value = 'pt') %>%
    unnest_wider(.data$pt, names_sep = '_') %>%
    mutate(pt_desc = tolower(.data$pt_lab),
           pt_lab = factor(.data$pt_lab, levels = unique(.data$pt_lab)),
           pt_col = factor(.data$pt_col, levels = class_pal))
}

obs_type_detailed <- function() {
  enframe(list(.estim = c(sub = 'tj'),
               .interp = c(sub = "'{'*j%in%J*','*k%in%K*'}'"),
               .extrap_j = c(sub = "'{'*j%notin%J*','*k%in%K*'}'"),
               .extrap_k = c(sub = "'{'*j%in%J*','*k%notin%K*'}'"),
               .extrap_jk = c(sub = "'{'*j%notin%J*','*k%notin%K*'}'")),
          name = 'obs_type', value = 'pt') %>%
    unnest_wider(.data$pt, names_sep = '_') %>%
    mutate(pt_col = brewer_pal(type = 'qual', palette = 'Set1')(n()),
           pt_col = factor(.data$pt_col, levels = .data$pt_col),
           pt_lab = ifelse(.data$obs_type == '.estim',
                           sprintf('italic(y[%s])', .data$pt_sub),
                           sprintf('bolditalic(y)*minute[italic(%s)]', .data$pt_sub)),
           pt_lab = factor(.data$pt_lab, levels = .data$pt_lab),
           pt_desc = sub('_', '-', sub('\\.', '', .data$obs_type))) %>%
    select(-.data$pt_sub)
}

part_type <- function() {
  # https://developers.google.com/machine-learning/crash-course/validation/another-partition
  ml_pal_google <- c(
    rgb(81, 134, 237, maxColorValue = 255),
    rgb(110, 194, 242, maxColorValue = 255),
    rgb(235, 182, 62, maxColorValue = 255)
  )
  tibble(partition = c('training', 'validation', 'test')) %>%
    mutate(pt_col = ml_pal_google, #brewer_pal(type = 'qual', palette = 'Paired')(n()),
           pt_col = factor(.data$pt_col, levels = .data$pt_col),
           pt_lab = str_to_title(.data$partition),
           pt_lab = factor(.data$pt_lab, levels = .data$pt_lab),
           pt_desc = tolower(.data$pt_lab))
}

fit_figure <- function(x) {
  tibble(columns = c(1, 1.5, 2), mm = c(85, 114, 174)) %>%
    mutate(`in` = .data$mm / 25.4) %>%
    filter(.data$columns == x) %>%
    pull(.data$`in`)
}

n_sim <- function(draws, n, n_max = 1000) {
  n_draws <- dim(draws[[1]])[1] * length(draws)
  ifelse(is.null(n), min(n_max, n_draws), min(n, n_draws))
}
