# greenwave
R package for fitting the __greenwave__ model to time series of vegetation indices

## Installation

You can install the development version of __greenwave__ with:

```R
devtools::install_gitlab("apis-staging/greenwave")
```

## Usage

For deploymenet in non-interactive settings, batch runs, automated testing of
various parameterizations, CI / CD etc. see the Greenwave Command Line Tool:

```sh
Rscript demo/gw-cli.R --help
Rscript demo/gw-sim.R --incl_sim_opts --n_locs 8
```

Note that we have repurposed the demo directory for internal use, including
simulation, conceptual diagrams, DAGs, etc. We make unapologetic use of some
of the package's internal functions, and point would-be users instead to the
vignettes.

## Abbreviations
The following abbreviations (including their invocation as prefixes / suffixes)
are used throughout the package.

| Abbreviation  |  Short for | Definition / example  |
|---|---|---|
| `n`  | number  | The number of years, `n_years`  |
| `gw`  | greenwave  | prefix for all functions exported by the package  |
| `re`  | random effects  |   |
| `sf`  | scaling factors  |   |
| `sd` / `sds`  | standard deviation(s)  |   |
| `id`  | ID  | alphanumeric identifier  |
| `idx`  | index  | sequential integer  |
| `loc`  | location  |   |
| `opts`  | options  |   |
| `cal`  | calendar  | Used exclusively to distinguish between _calendar_ (e.g., 1984, 1985, ..., 2002) and _relative_ years (0, 1, ..., 18) |
| `incl`  | include  |   |
| `excl`  | exclude  | `excl_param`  |

## Licensing and attribution

All material housed in this repository is licensed under the [GPLv3 license](https://gitlab.com/apis-staging/greenwave/-/blob/main/LICENSE.md) expect for the data-raw/figures directory, whose contents are licensed under the [Creative Commons Attribution-NonCommercial-ShareAlike 3.0](https://creativecommons.org/licenses/by-nc-sa/3.0/) license. When making use of those figures please link to this repository and note that the copyright is held by Conservation Science Partners, Inc.
