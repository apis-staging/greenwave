---
title: "Ikh Nart"
author: Luke Zachmann
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{Ikh Nart}
  %\VignetteEngine{knitr::knitr}
  \usepackage[utf8]{inputenc}
bibliography: citations.bib
csl: ecology.csl
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```

```
mkdir -p /datadrive/denver-zoo/ikh-nart /datadrive/cache/ikh-nart
gsutil -m rsync \
  -x '^GDDP.*' gs://ikh-nart/data/wdpa-99848 /datadrive/denver-zoo/ikh-nart
ln -s /datadrive/cache cache  # rsync -r /datadrive/cache/ ~/greenwave/cache
```

```{r setup, message=FALSE}
devtools::install_github("docopt/docopt.R")
devtools::install(quick = TRUE, upgrade = 'never')
library(tidyverse)
library(lubridate)
library(greenwave)

seed <- 123  # controls the data partition
n_locs <- 8  # number of locations
n_years <- 12  # number of years

n_burnin <- 3000
n_iter <- 3000
n_chains <- 5  # number of MCMC chains to run

eo_data <- 'LANDSAT_NDVI_EVI_wdpa-99848_v1_long_2020-06-01.csv'  # 'MODIS_wdpa-99848_v1_long_2020-05-24.csv'
sensor <- tolower(strsplit(eo_data, '_')[[1]][1])

cache <- file.path('/datadrive/cache/ikh-nart', sensor)  #'/tmp/cache/ikh-nart'
assets_path <- '/datadrive/denver-zoo/ikh-nart'  #'../data-raw/denver-zoo/ikh-nart'

include_drivers <- FALSE
```

```{r tuning}
modis_ndvi_re_sf <-
  "c(lapply(greenwave:::get_re_sf_defaults()[c('alpha1')], function(x) x * 14),
  lapply(greenwave:::get_re_sf_defaults()[c('delta1')], function(x) x * 16),
  lapply(greenwave:::get_re_sf_defaults()[c('delta2')], function(x) x * 4),
  lapply(greenwave:::get_re_sf_defaults()[c('gamma2')], function(x) x * 10),
  lapply(greenwave:::get_re_sf_defaults()[c('lambda')], function(x) x / 2))"
landsat_ndvi_re_sf <-
  "c(lapply(greenwave:::get_re_sf_defaults()[c('alpha1')], function(x) x * 18),
  lapply(greenwave:::get_re_sf_defaults()[c('delta1')], function(x) x * 15*1.5),
  lapply(greenwave:::get_re_sf_defaults()[c('delta2')], function(x) x * 4),
  lapply(greenwave:::get_re_sf_defaults()[c('gamma1_R')], function(x) x * 3),
  lapply(greenwave:::get_re_sf_defaults()[c('gamma2')], function(x) x * 12))"
ndvi_priors <- 
  "list(alpha1=list(mean = 200, sd = 50),
       delta1 = list(mean = 1/30, sd = greenwave:::get_sd_delta(30,shift = 5)),
       delta2 = list(mean = -1/30, sd = greenwave:::get_sd_delta(30,shift = 5)),
       gamma1_R = list(mean = 0.1, sd = 0.05),
       gamma2 = list(mean = 0.2, sd = 0.05),
       lambda = list(mean = log(275-200), sd = 0.5))"  # plot(density(exp(rnorm(1E4, log(275-200), 0.5))))
```

```{r opts-noninteractive, echo=FALSE, message=FALSE}
ndvi_re_sf <- if (sensor == 'modis') modis_ndvi_re_sf else landsat_ndvi_re_sf
if (!interactive()) {
  results_dir <- system(
    # Command line call to create opts non-interactively.
    sprintf('Rscript helper.R \\
                  --seed %s --n_locs %s --n_years %s \\
                  --use_single_spacetime \\
                  --excl_params %s \\
                  --n_burnin %s --n_iter %s --n_chains %s \\
                  --resp_data %s --cov_data %s \\
                  --cache %s --omit_sys_time \\
                  --loc_attr %s --dttm_attr %s \\
                  --p_parts %s \\
                  --re_sf %s \\
                  --priors %s',
            seed, n_locs, n_years,
            shQuote("c('omega2','omega1_R')"),
            n_burnin, n_iter, n_chains,
            file.path(assets_path, eo_data), 'none',
            cache,
            "loc_id", 'img_date',
            shQuote("c(training = 0.9, test = 0.1)"),
            shQuote(ndvi_re_sf),
            shQuote(ndvi_priors)
            ),
    intern = TRUE)
  # results_dir <- 'cache/ikh-nart/75bc8f80'  # temporary
  opts <- readRDS(file.path(results_dir, 'opts.meta'))
  print(opts)
}
```

```{r opts-interactive}
if (interactive()) {
  opts <- gw_opts(
    seed = seed, n_locs = n_locs, n_years = n_years,
    use_single_spacetime = TRUE,
    excl_params = 'c("omega1_R", "omega2")',
    n_burnin = n_burnin, n_iter = n_iter, n_chains = n_chains,
    resp_data = file.path(assets_path, eo_data), cov_data = 'none',
    cache = cache, omit_sys_time = TRUE,
    loc_attr = "loc_id", dttm_attr = 'img_date',
    p_parts = c(training = 0.9, test = 0.1),
    re_sf = ndvi_re_sf,
    priors = ndvi_priors
  )
  # opts <- readRDS('/datadrive/cache/ikh-nart/c63e0388/opts.meta')
  # opts <- readRDS('/datadrive/cache/ikh-nart/a5d41d0d/opts.meta')
}
print(opts)
print(opts$re_sf)
print(opts$priors)
```

# The response data

```{r resp-data}
which_vi <- "NDVI"
if (sensor == 'modis') {
  # Update image date using DayOfYear (the actual aqcuisition day) for MODIS.
  resp_data_raw <- opts$resp_data %>%
    mutate(img_date = ymd(parse_date_time(paste(year(img_date), DayOfYear),
                                          'Y! j'))) %>%
    select(-DayOfYear)
  other_keepers <- NULL
} else if (sensor == 'landsat') {
  resp_data_raw <- opts$resp_data %>% filter(sensor %in% c(5, 7))
  other_keepers <- c('sensor', 'pathrow')
}

# Load response and covariate data.
resp_data <- resp_data_raw %>%
  filter(year(date) < 2020, get(which_vi) > 0) %>%
  gw_partition_vi(opts$cov_data, opts, vi = !!which_vi,
                  date = !!opts$dttm_attr, loc_id = !!opts$loc_attr,
                  other_keepers = other_keepers) %>%
  group_by(loc_id, day) %>%  # this is new!
  sample_n(1) %>%
  ungroup()
Y_in_sample <- resp_data %>% filter(partition == 'training')
dim(Y_in_sample)
```

```{r vis-resp-data, include=FALSE}
obs <- gw_vis_obs(resp_data, cache_dir = opts$cache, file_ext = 'png')
```

```{r, echo=FALSE, fig.align="center", out.width = '100%'}
knitr::include_graphics(file.path(opts$cache, "data", "obs-type-detailed/estim.png"))
```

# Covariate data

```{r cov-data}
effects <- list(alpha1 = c('pr_JFM', 'pr_AMJ', 'tmmx_AMJ'),  # 'pr_JFM', 'pr_AMJ'
                # delta1 = c('pdsi_AMJ'),
                gamma1_R = c('elevation', 'tmmn_OND'),
                gamma2 = c('elevation'))

drivers <- greenwave:::make_dacqre(assets_path,
            loc_attr = "loc_id", met_src = 'TERRACLIMATE')  # opts$loc_attr
Xs_in_sample <- gw_format_covariates(
  data = drivers,
  Y = Y_in_sample, #%>% filter(year <= 2019),  # ugh failsaife
  effects = effects,
  loc_id = 'location',  # opts$loc_attr
  cal_year = "year",
  to_z_score = TRUE
)
if (!include_drivers) Xs_in_sample <- NULL
str(Xs_in_sample)
```

```{r mod, message=FALSE}
draws_ckpt <- file.path(opts$cache, 'draws.ckpt')
draws <- if (file.exists(draws_ckpt)) {
  readRDS(draws_ckpt)
} else {
  gw_fit(Y_in_sample, Xs = Xs_in_sample, opts)
}
```

```{r trace}
traces <- gw_vis_traces(draws, coefs = 'fixed', cache_dir = opts$cache, file_ext = 'png')
```

```{r vis-trace, echo=FALSE, fig.align="center", out.width = '100%'}
knitr::include_graphics(file.path(opts$cache, "trace", "params.png"))
```

```{r sf}
sf_eval <- gw_param_scaled_dists(draws, opts$cache, file_ext = 'png')
```

```{r vis-sf, echo=FALSE, fig.align="center", out.width = '100%'}
knitr::include_graphics(file.path(opts$cache, "sf", "sd-kappa-param-scaled-dists.png"))
```


<!-- # Model evaluation -->
<!-- ```{r pred} -->
<!-- ppds_data <- file.path(opts$cache, 'pred', 'ppds.data') -->
<!-- ppds <- if (file.exists(ppds_data)) { -->
<!--   readRDS(ppds_data) -->
<!-- } else { -->
<!--   gw_ppd(draws, cache_dir = opts$cache)  # calculate once, recycled below! -->
<!-- } -->

<!-- pred <- gw_vis_fit(ppds, opts, n_yhat_draws = 100, cache_dir = opts$cache, -->
<!--                    file_ext = 'png') -->
<!-- # po <- gw_vis_po(ppds, opts, cache_dir = opts$cache, file_ext = 'png') -->
<!-- po <- gw_vis_po(ppds, opts, cache_dir = opts$cache, Y = resp_data, file_ext = 'png') -->
<!-- ``` -->

<!-- ```{r vis-pred, echo=FALSE, fig.align="center", out.width = '100%'} -->
<!-- knitr::include_graphics(file.path(opts$cache, "pred", "yhat-complete-with-data.png")) -->
<!-- ``` -->

<!-- ```{r vis-skill, echo=FALSE, fig.align="center", out.width = '100%'} -->
<!-- knitr::include_graphics(file.path(opts$cache, "skill", "pred-obs-test.png")) -->
<!-- ``` -->

<!-- ```{r diag} -->
<!-- diagnostics <- gw_diagnostics(draws, cache_dir = opts$cache) -->
<!-- knitr::kable(diagnostics, digits = 2) -->
<!-- ``` -->

```{r dqs, message=FALSE}
dqs_data <- file.path(opts$cache, 'dqs', 'dqs.data')
dqs <- if (file.exists(dqs_data)) {
  readRDS(dqs_data)
} else {
  gw_dqs(draws, other_params = greenwave:::param_names(), cache_dir = opts$cache, n_samples = 250,
         batch_size = 50)
  # gw_dqs(draws, other_params = greenwave:::param_names(), cache_dir = opts$cache)
}

# This is TMI!
# dq_density <- gw_vis_density(dqs, cache_dir = opts$cache,
#                which_params = c(greenwave:::param_names(), 'alpha2'),
#                which_effects = 'fixed_plus_random',  # c('fixed_plus_random', 'intercept')
#                by = 'group', file_ext = 'png')
```

```{r more-whiskers, include = FALSE}
dqs_tidy <- gw_dqs_tidy(dqs)
dq_just_params <- dqs_tidy$fixed_plus_random %>%
  filter(dq %in% c(greenwave:::param_names(), 'alpha2')) %>%
  gw_vis_dqs(cache_dir = opts$cache, suffix = 'just-params', file_ext = 'png', n_cols = 1)
focal_dqs <- c('sos', 'accum_vi_over_min', 'n_days_over_thresh')
dq_other_errbars <- dqs_tidy$fixed_plus_random %>%
  filter(dq %in% focal_dqs) %>% 
  # filter(!dq %in% c(greenwave:::param_names(), 'alpha2')) %>%
  gw_vis_dqs(cache_dir = opts$cache, suffix = 'extra', file_ext = 'png', n_cols = 1)
```

<!-- ```{r vis-more-whiskers, echo=FALSE, fig.align="center", out.width = '100%'} -->
<!-- knitr::include_graphics(file.path(opts$cache, "dqs/whisker-plots/dqs-extra.png")) -->
<!-- ``` -->

<!-- ```{r mining, debug=TRUE} -->
<!-- dqs_random_effects <- gw_dqs(draws, which_effects = 'random', -->
<!--                              other_params = greenwave:::param_names(), -->
<!--                              cache_dir = sub('tmp', 'datadrive', opts$cache), -->
<!--                              suffix = 're') -->
<!-- # gw_vis_resid(dqs_random_effects, Xs = sim$Xs) -->
<!-- ``` -->

<!-- ```{r drivers, debug=TRUE, message=FALSE, warning=FALSE} -->
<!-- # source('../R/.dacqre.R') -->
<!-- drivers <- make_dacqre('/datadrive/denver-zoo/ikh-nart', -->
<!--             loc_attr = "loc_id", met_src = 'TERRACLIMATE')  # opts$loc_attr -->
<!-- ``` -->

<!-- ```{r} -->
<!-- devtools::load_all() -->
<!-- # month_for_yday(200); get_ww_lookup() -->
<!-- effects <- list(alpha1 = grep('^.*_(JFM|AMJ)$', names(drivers), value = TRUE), -->
<!--                 lambda = grep('^.*_(AMJ|JAS)$', names(drivers), value = TRUE), -->
<!--                 gamma1_R = grep('^.*_(OND)$', names(drivers), value = TRUE), -->
<!--                 gamma2 = grep('^.*_(JFM|AMJ)$', names(drivers), value = TRUE), -->
<!--                 delta1 = grep('^.*_(JFM|AMJ)$', names(drivers), value = TRUE), -->
<!--                 delta2 = grep('^.*_(JAS|OND)$', names(drivers), value = TRUE))  # alpha1 = 'year' -->
<!-- effects <- lapply(effects, function(x) { -->
<!--   unique(c(x, c('elevation', 'northness', 'eastness', 'lat'))) -->
<!-- }) -->
<!-- Xs_speculative <- gw_format_covariates( -->
<!--   data = drivers, -->
<!--   Y = Y_in_sample, #%>% filter(year <= 2019),  # ugh failsaife -->
<!--   effects = effects, -->
<!--   loc_id = 'location',  # opts$loc_attr -->
<!--   cal_year = "year", -->
<!--   to_z_score = TRUE -->
<!-- ) -->
<!-- ``` -->

<!-- ```{r, message=FALSE, warning=FALSE} -->
<!-- # dqs_random_effects <- gw_dqs(draws, which_effects = 'random', other_params = greenwave:::param_names(), cache_dir = opts$cache, suffix = 're') -->
<!-- unexplained <- gw_vis_resid(dqs_random_effects, Xs = Xs_speculative, -->
<!--                             cache_dir = opts$cache) -->

<!-- # effects <- list(alpha1 = c('pdsi_AMJ'),  # 'pr_JFM', 'pr_AMJ' -->
<!-- #                 delta1 = c('pdsi_AMJ'), -->
<!-- #                 gamma1_R = c('elevation'), -->
<!-- #                 gamma2 = c('elevation')) -->
<!-- # gw_vis_X(Xs_speculative, envir = attr(draws, "fit_info")) -->
<!-- ``` -->
