% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/simulate.R
\name{gw_simulate_obs}
\alias{gw_simulate_obs}
\title{Simulate greenwave observations}
\usage{
gw_simulate_obs(
  opts,
  sigma_y = 0.0075,
  params = NULL,
  n_forecast_years = 0,
  ...
)
}
\arguments{
\item{opts}{Run settings, including options for pre-processing / filtering
data, RNG state, model parameterization, implementation, and evaluation, and
results caching. A list. See \code{\link[=gw_opts]{gw_opts()}} for details.}

\item{sigma_y}{The standard deviation of simuated data points.}

\item{params}{Optional list of user-supplied greenwave parameter values.
Defaults to \code{NULL}, in which case the default values that are "baked-in" to
the internal function \code{gw_simulate_mean()} are used.}

\item{n_forecast_years}{Optional number of years for forecasting. Used to
generate covariates, but not observations. Defaults to 0.}

\item{...}{Additional optional arguments (to be passed to
\code{gw_simulate_mean()}).}
}
\value{
A list containing the response data as well as a list of design
matrices for parameter models.
}
\description{
\code{gw_simulate_obs()} makes "fake" greenwave data (time series of vegetation
indices) according to a set of options, the defaults for which are specified
by \code{\link[=gw_opts]{gw_opts()}}. Several decisions regarding the presence / absence and
strength of fixed effects have been "baked in" to this function, but could
easily be hacked to yield different – simpler, more complicated, or
more challenging – datasets.
}
\examples{
opts <- gw_opts(incl_sim_opts = TRUE)
sim <- gw_simulate_obs(opts)
}
